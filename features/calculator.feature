Feature: Test Calculator

Scenario: Try to 2 + 2
  Given I use Calculator class
  When I apply operation "reset"
    And I enter number "2"
    And I apply operation "add"
    And I enter number "2"
    And I apply operation "add"
  Then I will get "4"
#  And I enter first number
#  And I enter 2 as first number
#  And I enter 2 as second number
#  When I apply operation "add"
#  Then I should get: "4"