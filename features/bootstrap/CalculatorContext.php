<?php
use Behat\MinkExtension\Context\MinkContext;

class CalculatorContext extends MinkContext
{
    private $calc;

    /**
     * @Given /^I use Calculator class$/
     */
    public function iUseCalculatorClass()
    {
        $this->calc = new \AppBundle\Utils\Calculator();
    }

    /**
     * @Given /^I apply operation "([^"]*)"$/
     */
    public function iApplyOperation($operation)
    {
        $this->calc->apply($operation);
    }

    /**
     * @Given /^I enter number "([^"]*)"$/
     */
    public function iEnterNumber($number)
    {
        $this->calc->enter($number);
    }

    /**
     * @Then /^I will get "([^"]*)"$/
     */
    public function iWillGet($number)
    {
        if ($this->calc->result != $number) {
            throw new Exception(
                "Calculator res is: {$this->calc->result}, but you expect: {$number}"
            );
        }
    }

}