Feature: Search
  In order to see a word definition
  As a website user
  I need to be able to search for a word

  Scenario: Just get the main page
    Given I am on "/"
    Then I should see "A Warm Welcome"

  Scenario: Go to products page
    Given I am on "/ru/foods"
    Then I should see "Список продуктов"
    When I fill in "search" with "молоко"
    And I press "searchButton"
    Then I should see "молоко"

#  Scenario: Searching for a page that does exist
#    Given I am on "/"
#    When I fill in "search" with "молоко"
#    And I press "searchButton"
#    Then I should see "молоко"

#  Scenario: Searching for a page that does NOT exist
#    Given I am on "/wiki/Main_Page"
#    When I fill in "search" with "Glory Driven Development"
#    And I press "searchButton"
#    Then I should see "Search results"
