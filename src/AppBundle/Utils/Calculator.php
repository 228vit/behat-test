<?php

namespace AppBundle\Utils;

class Calculator
{
    public $first_number;
    public $entered_number;
    public $result;

    public function enter($number)
    {
        return $this->entered_number = $number;
    }

    public function apply($operation)
    {
        switch ($operation) {
            case "add":
                $this->result += $this->entered_number;
                break;
            case "extract":
                $this->result -= $this->entered_number;
                break;
            case "multiply":
                $this->result = $this->result * $this->entered_number;
                break;
            case "divide":
                $this->result = $this->result / $this->entered_number;
                break;
            case "reset":
                $this->result = 0;
                break;
            default:
                throw \Exception('Unknown operation: '.$operation);
        }

        return $this->result;
    }

}